<%-- 
    Document   : home
    Created on : Feb 9, 2023, 11:26:00 PM
    Author     : ThinkPro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Home page</title>
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/fonts/themify-icons/themify-icons.css">
        <%@include file="component/javascript.jsp" %>
    </head>
    <body>

        <%@include file="component/header.jsp" %>
        <%@include file="component/account.jsp" %>

        <!-- body content -->
        <div id="content">
            <div class="section mcb-section mcb-section-80cba8ba8 dark" 
                 style="padding-top:90px;padding-bottom:40px; height: 800px; width: 100%;
                 background-color:#1e1d23;
                 background-image:url('images/homebackground.jpg')
                 ; background-repeat:no-repeat;background-position:0px 50px; background-size: 100% 100%;
                 ">
                <div class="section_wrapper mcb-section-inner">
                    <div class="slogan" style="margin: 70px 10% 0 0;">
                        <h2 style="font-size: 55px; line-height: 55px; margin-bottom: 35px; margin-left: 700px; color: greenyellow">
                            Keep your body<br>fit &amp;  strong</h2>  

                        <h3 style="font-size: 30px; line-height: 35px; margin-bottom: 50px; margin-left: 700px; color: cornsilk ">
                            Chúng tôi có nhiều sản phẩm, dịch vụ và tâm huyết giúp sức khỏe và vóc dáng của bạn tốt hơn! </h3>
                        <h3 style="font-size: 30px; line-height: 35px; margin-bottom: 50px; margin-left: 700px; color: lightcyan ">
                            Thời gian hoạt động: 8:00AM - 22:00PM  </h3>

                        <!--                        <a class="button button_size_2 button_theme" href="#">
                                                    <span class="button_label">Start today</span>
                                                </a>-->

                    </div>
                </div>
            </div>

        </div>
    </body>
</html>
