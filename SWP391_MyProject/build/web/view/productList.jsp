<%-- 
    Document   : productList
    Created on : Feb 12, 2023, 10:15:22 PM
    Author     : ThinkPro
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="assets/css/style.css">
        <!--        <link rel="stylesheet" href="./assets/fonts/themify-icons/themify-icons.css">-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <title>Product List</title>
        <%@include file="../component/javascript.jsp" %>


    </head>
    <body>
        <div id="main">

            <%@include file="../component/header.jsp" %>
            <%@include file="../component/account.jsp" %>

            <div class="container-fluid" style="margin-top: 100px ">
                <div class="row">
                    <c:forEach items="${listOfPage}" var="p">
                        <div class="col-lg-2 col-md-3 col-sm-6 product-down">
                            <div class="row">
                                <div class="product-item">
                                    <div class="product-top">
                                        <a href="list-detail?product_id=${p.product_id}&cp_id=${p.cp_id}" class="product-thumb">
                                            <img style="height: 370px; width: 200px;" src="${p.images.get(0)}"alt="">
                                        </a>
                                        <c:if test="${sessionScope.us == null}" >
                                            <a class="buy-now" data-toggle="modal"  data-target="#loginModal" style="color: white">Mua ngay</a>
                                        </c:if>
                                        <c:if test="${sessionScope.us != null}" >
                                            <a href="addcart?product_id=${p.product_id}" class="buy-now" >Mua ngay</a>
                                        </c:if>
                                    </div>
                                    <div class="product-infor">
                                        <a href="" class="product-name">${p.product_name}</a>
                                        <div class="product-price">
                                            <c:if test="${p.sale_price != 0}">
                                                ${p.sale_price}đ
                                                <del>${p.original_price}đ</del>
                                            </c:if>
                                            <c:if test="${p.sale_price == 0}">
                                                ${p.original_price}đ
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>



                <!<!-- pagination -->
                <nav aria-label="..." class="pagination">
                    <ul class="pagination">
                        <li class="page-item">
                            <a <c:if test="${pageIndex!=1}">                         
                                    href="list-product?index=${pageIndex-1}"
                                </c:if> class="page-link" aria-label="Previous">
                                <span  aria-hidden="true">«</span>
                            </a>
                        </li>

                        <c:forEach begin="1" end="${endPage}" var="i">
                            <li class="page-item ${i==pageIndex ?"active" : ""}"><a class="page-link" href="list-product?index=${i}">${i}</a></li>
                            </c:forEach>

                        <li class="page-item">
                            <a <c:if test="${pageIndex!=endPage}">
                                    href="list-product?index=${pageIndex+1}"
                                </c:if> class="page-link" aria-label="Next">
                                <span aria-hidden="true">»</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

        </div>

    </body>
</html>
