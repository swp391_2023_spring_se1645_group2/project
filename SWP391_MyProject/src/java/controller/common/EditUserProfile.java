/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.TimeUnit;
import model.User;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author ThinkPro
 */
public class EditUserProfile extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditUserProfile</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditUserProfile at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final long MAX_FILE_SIZE = 1024 * 1024;
    private static final List<String> ALLOWED_EXTENSIONS = Arrays.asList("jpg", "jpeg", "png");

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (!ServletFileUpload.isMultipartContent(request)) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Expected multipart content");
            return;
        }

        HttpSession session = request.getSession();
        UserDAO ud = new UserDAO();
        // Create a factory for disk-based file items
        DiskFileItemFactory factory = new DiskFileItemFactory();

        // Set factory constraints
        factory.setSizeThreshold((int) MAX_FILE_SIZE);

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // Set maximum file size to 10 MB
        upload.setSizeMax(MAX_FILE_SIZE);

        // Parse the request
        try {
            List<FileItem> items = upload.parseRequest((RequestContext) request);
            for (FileItem item : items) {
                if (!item.isFormField()) {
                    // Process the uploaded file item
                    String fileName = item.getName();
                    if (!isAllowedFileExtension(fileName)) {
                        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid file extension");
                        return;
                    }
                    // Encode the image to Base64
                    byte[] imageBytes = item.get();
                    System.out.println("aaab");
                    String imageData = "data:image/png;base64," + Base64.getEncoder().encodeToString(imageBytes);
                    // Do something with the image, such as save it to the database
                    String fullName = request.getParameter("fullName");
                    String user_idraw = request.getParameter("user_id");
                    String email = request.getParameter("email");
                    String phone = request.getParameter("phone");
                    String gender = request.getParameter("gender");
                    String address = request.getParameter("address");
                    int uid = Integer.parseInt(user_idraw);
                    System.out.println(fullName + "fff");
                    ud.editUserProfile(fullName, imageData, gender, phone, address, uid);

                    User u = ud.getUserById(uid);
                    session.setAttribute("us", u);
                    response.sendRedirect("home");
                }
            }
        } catch (FileUploadException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "File upload failed");
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean isAllowedFileExtension(String fileName) {
        String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
        return ALLOWED_EXTENSIONS.contains(fileExtension);
    }

}
