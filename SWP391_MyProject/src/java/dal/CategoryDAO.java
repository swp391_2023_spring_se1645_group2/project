/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Locale.Category;
import model.CategoryProduct;
import model.Product;

/**
 *
 * @author ThinkPro
 */
public class CategoryDAO extends context.DBContext<Product> {

    @Override
    public ArrayList<Product> list() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Product get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void insert(Product model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(Product model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(Product model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public ArrayList<CategoryProduct> getProductCategories() {
        ArrayList<CategoryProduct> list = new ArrayList();
        String sql = "SELECT [cp_id]\n"
                + "      ,[cp_name]\n"
                + "      ,[status]\n"
                + "  FROM [dbo].[CategoryProduct]";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CategoryProduct c = CategoryProduct.builder().cp_id(rs.getInt((1)))
                        .cp_name(rs.getString(2))
                        .status(rs.getBoolean(3)).build();
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    CategoryProduct getCategoryByID(int cid) {
        String sql = "SELECT * "
                + "  FROM [dbo].[CategoryProduct]\n"
                + "  where cp_id=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, cid);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                CategoryProduct c = new CategoryProduct(rs.getInt("cp_id"), rs.getString("cp_name"), rs.getBoolean("status"));
                return c;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<CategoryProduct> getAllCategory() {

        ArrayList<CategoryProduct> list = new ArrayList<>();
        String sql = "Select * from Category where status = 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CategoryProduct c = CategoryProduct.builder()
                        .cp_id(rs.getInt(1))
                        .cp_name(rs.getString(2))
                        .status(rs.getBoolean(3))
                        .build();

                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

}
