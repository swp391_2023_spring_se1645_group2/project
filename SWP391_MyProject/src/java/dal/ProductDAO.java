/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.CategoryProduct;
import model.Product;

/**
 *
 * @author ThinkPro
 */
public class ProductDAO extends context.DBContext<Product> {

    @Override
    public ArrayList<Product> list() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Product get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void insert(Product model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(Product model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(Product model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public ArrayList<String> getProductImage(int product_id) {
        ArrayList<String> list = new ArrayList();
        try {
            String sql = "select image_url from  Product_Image pi  join Image i on i.image_id = pi.image_id where pi.product_id = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, product_id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("image_url"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<Product> getAllProduct() {
        ArrayList<Product> list = new ArrayList<>();
        String sql = "SELECT * "
                + "  FROM [dbo].[Product]";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ArrayList<String> l = getProductImage(rs.getInt("product_id"));
                Product p = Product.builder()
                        .product_id(rs.getInt(1))
                        .product_name(rs.getString(2))
                        .original_price(rs.getInt(3))
                        .sale_price(rs.getInt(4))
                        .pDescription(rs.getString(5))
                        .brief_info(rs.getString(6))
                        .quantity(rs.getInt(7))
                        .status(rs.getBoolean(8))
                        .cp_id(rs.getInt(9))
                        .update_date(rs.getDate(10))
                        .images(getProductImage(rs.getInt(1)))
                        .build();
                list.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public int getTotalProduct() {
        String sql = "select COUNT(product_id) from Product";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public int getTotalPublishedProduct() {
        String sql = "select COUNT(product_id) from Product where status = 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public ArrayList<Product> search(String keyword, int cid) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT *"
                + "  FROM [dbo].[Product] "
                + "where 1=1";
        if (keyword != null && !keyword.equals("")) {// khac null và có ký tự nhập vào
            sql += " and product_name like '%" + keyword + "%' ";
        }

        if (cid != 0) {
            sql += " and cp_id=" + cid;
        }
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            CategoryDAO d = new CategoryDAO();
            while (rs.next()) {
                CategoryProduct c = d.getCategoryByID(cid);
                ArrayList<String> l = getProductImage(rs.getInt("product_id"));
                Product p = Product.builder()
                        .product_id(rs.getInt(1))
                        .product_name(rs.getString(2))
                        .original_price(rs.getInt(3))
                        .sale_price(rs.getInt(4))
                        .pDescription(rs.getString(5))
                        .brief_info(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .quantity(rs.getInt(8))
                        .cp_id(rs.getInt(9))
                        .update_date(rs.getDate(10))
                        .images(l)
                        .build();

                list.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<Product> pagingProduct(int index, int recordPerPage) { // index: trang click
        ArrayList<Product> list = new ArrayList<>();
        String query = "select * from Product order by product_id OFFSET ? ROWs fetch next ? rows only;"; // bat dau tu dong index, moi lan in ra recourdPerpage ban ghi
        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, (index - 1) * recordPerPage);
            ps.setInt(2, recordPerPage);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // ArrayList<String> l = getProductImage(rs.getInt("product_id"));
                Product p = Product.builder()
                        .product_id(rs.getInt(1))
                        .product_name(rs.getString(2))
                        .original_price(rs.getFloat(3))
                        .sale_price(rs.getFloat(4))
                        .pDescription(rs.getString(5))
                        .brief_info(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .quantity(rs.getInt(8))
                        .cp_id(rs.getInt(9))
                        .update_date(rs.getDate(10))
                        .images(getProductImage(rs.getInt(1)))
                        .build();

                list.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Product getProductNew() {
        String sql = "select top 1 * from Product order by update_date desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                // ArrayList<String> l = getProductImage(rs.getInt("product_id"));
                Product p = Product.builder()
                        .product_id(rs.getInt(1))
                        .product_name(rs.getString(2))
                        .original_price(rs.getInt(3))
                        .sale_price(rs.getInt(4))
                        .pDescription(rs.getString(5))
                        .brief_info(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .quantity(rs.getInt(8))
                        .cp_id(rs.getInt(9))
                        .update_date(rs.getDate(10))
                        .images(getProductImage(rs.getInt(1)))
                        .build();

                return p;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
}
