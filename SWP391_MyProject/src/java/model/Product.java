/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.util.ArrayList;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
/**
 * /**
 *
 * @author ThinkPro
 */
public class Product {

    private int product_id;
    private String product_name;
    private float original_price;
    private float sale_price;
    private String pDescription;
    private String brief_info;
    private int quantity;
    private boolean status;
    private int cp_id;
    private Date update_date;
    private ArrayList<String> images;

}
