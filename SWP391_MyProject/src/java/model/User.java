/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
/**
 *
 * @author ThinkPro
 */
public class User {

    private int user_id;
    private String fullName;
    private String password;
    private String avatar;
    private boolean gender;
    private String email;
    private String phone;
    private String address;
    private boolean status;
    private int role_id;

}
