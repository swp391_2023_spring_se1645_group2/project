<%-- 
    Document   : header
   Created on : Feb 3, 2022
    Author     : Thinkpro
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div id="header">
    <nav class="navbar navbar-expand-lg navbar-header bg-body">
        <div class="container-fluid " >
            <nav class="navbar navbar-light bg-light">
                <a class="navbar-brand " href="home">
                    <img src="images/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                    FitnessCare
                </a>
            </nav>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>



            <div class="collapse navbar-collapse ms-5" id="navbarSupportedContent">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="link" href="home" role="tab" aria-controls="nav-home" aria-selected="true">Trang chủ</a> &nbsp; &nbsp;
                        <a class="nav-item nav-link " id="nav-profile-tab" data-toggle="link" href="list-product" role="tab" aria-controls="nav-profile" aria-selected="false">Sản phẩm</a> &nbsp; &nbsp;
                        <a class="nav-item nav-link " id="nav-contact-tab" data-toggle="link" href="list-service" role="tab" aria-controls="nav-contact" aria-selected="false">Dịch vụ</a> &nbsp;&nbsp;
                        <a class="nav-item nav-link " id="nav-contact-tab" data-toggle="link" href="list-blog" role="tab" aria-controls="nav-contact" aria-selected="false">Blog</a>&nbsp;&nbsp;
                        <a class="nav-item nav-link " id="nav-contact-tab" data-toggle="link" href="contact" role="tab" aria-controls="nav-contact" aria-selected="false">Liên hệ</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </nav>

                <nav class="navbar navbar-light bg-body">
                    <form class="form-inline">
                        <input style="font-size: 15px; pl" class="form-control mr-sm-2" name ="key" type="search" placeholder=" product or service" aria-label="Search" value="${key}" id="" required class="form-control">
                       &nbsp;&nbsp; <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>&nbsp;&nbsp;&nbsp;
                    </form>
                </nav>


                <ul class="navbar-nav mb-2 mb-lg-0">
                    <c:if test="${sessionScope.us != null}">
                        <div class="btn-group">
                            <button type="button" style="border-radius: 4px" class="btn btn-outline-dark py-2 px-4" data-toggle="dropdown" aria-expanded="false">
                                <c:if test="${sessionScope.us.avatar != null && sessionScope.us.avatar ne ''}">
                                    <img class="rounded-circle" width="20px" src="${sessionScope.us.avatar}">
                                    <span class="font-weight-bold" style="font-size: 16px">${sessionScope.us.fullName}</span>
                                </c:if>
                                <c:if test="${sessionScope.us.avatar == null || sessionScope.us.avatar eq ''}">
                                    <img class="rounded-circle" width="20px" height="20px" src="../images/avatar/avtdefault.png">
                                    <span class="font-weight-bold" style="font-size: 16px">${sessionScope.us.fullName}</span>
                                </c:if>
                            </button>


                            <ul class="dropdown-menu menuScroll">
                                <a type="button" data-toggle="modal" data-dismiss="modal" data-target="#userProfileModal" 
                                   > &nbsp;&nbsp; Thông tin cá nhân  </a>


                                <c:if test="${sessionScope.us.role_id == 1}">
                                    <li><a class="dropdown-item"  href="admin-manager">Quản lý  </a></li>
                                    </c:if>
                                    <c:if test="${sessionScope.us.role_id == 2 }">
                                    <li><a class="dropdown-item"  href="onlinestaff-manager">Quản lý  </a></li>
                                    </c:if>
                                    <c:if test="${sessionScope.us.role_id == 3 }">
                                    <li><a class="dropdown-item" href="tecnicalstaff-manager">Quản lý</a></li>
                                    </c:if>
                                    <c:if test="${sessionScope.us.role_id == 4}">
                                    <li><a class="dropdown-item" href="customer-manager">Quản lý</a></li>
                                    </c:if>
                                <li><a class="dropdown-item" href="logout">Đăng xuất</a></li>
                            </ul>
                        </div>
                    </c:if>
                    <c:if test="${sessionScope.us == null}">
                        <li class="nav-item">
                            <a><i type="button" class="ti-user btn btn-icon py-2 px-4" data-toggle="modal"  data-target="#loginModal"></i></a>
                        </li>
                    </c:if>
                    <!-- begin icon header -->
                    <c:if test="${sessionScope.us.role_id == 4 || sessionScope.us == null}">
                        <li class="nav-item">
                            <a class="nav-link btn btn-icon py-2 px-4" href="carts" tabindex="-1" aria-disabled="true"><i class="ti-shopping-cart"></i></a>
                        </li>
                    </c:if>
                </ul>
            </div>
        </div>
    </nav>
</div>




